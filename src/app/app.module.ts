import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { RootComponent } from './root/root.component';
import { Tab1Component } from './component/tab1/tab1.component';
import { Tab2Component } from './component/tab2/tab2.component';
import { Tab3Component } from './component/tab3/tab3.component';
import { Tab1Child1Component } from './component/tab1/tab1-child1/tab1-child1.component';
import { Tab1Child2Component } from './component/tab1/tab1-child2/tab1-child2.component';
import { Tab1Child3Component } from './component/tab1/tab1-child3/tab1-child3.component';
import { Tab2Chil1Component } from './component/tab2/tab2-chil1/tab2-chil1.component';
import { Tab2Chil2Component } from './component/tab2/tab2-chil2/tab2-chil2.component';
import { Tab2Chil3Component } from './component/tab2/tab2-chil3/tab2-chil3.component';
import { Tab3Child1Component } from './component/tab3/tab3-child1/tab3-child1.component';
import { Tab3Child2Component } from './component/tab3/tab3-child2/tab3-child2.component';
import { Tab3Child3Component } from './component/tab3/tab3-child3/tab3-child3.component';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    Tab1Component,
    Tab2Component,
    Tab3Component,
    Tab1Child1Component,
    Tab1Child2Component,
    Tab1Child3Component,
    Tab2Chil1Component,
    Tab2Chil2Component,
    Tab2Chil3Component,
    Tab3Child1Component,
    Tab3Child2Component,
    Tab3Child3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AccordionModule,
    TabViewModule,
    ButtonModule,
    TableModule,
    HttpClientModule,
    FormsModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    ReactiveFormsModule
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
