import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Tab1Child2Component } from './tab1-child2.component';

describe('Tab1Child2Component', () => {
  let component: Tab1Child2Component;
  let fixture: ComponentFixture<Tab1Child2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Tab1Child2Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Tab1Child2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
