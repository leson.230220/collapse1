import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Tab1Child3Component } from './tab1-child3.component';

describe('Tab1Child3Component', () => {
  let component: Tab1Child3Component;
  let fixture: ComponentFixture<Tab1Child3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Tab1Child3Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Tab1Child3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
