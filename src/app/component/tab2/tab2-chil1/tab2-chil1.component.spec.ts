import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Tab2Chil1Component } from './tab2-chil1.component';

describe('Tab2Chil1Component', () => {
  let component: Tab2Chil1Component;
  let fixture: ComponentFixture<Tab2Chil1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Tab2Chil1Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Tab2Chil1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
