import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges,} from '@angular/core';
import { statusCollapse } from '../../../types/config';

@Component({
  selector: 'app-tab2-child1',
  templateUrl: './tab2-chil1.component.html',
  styleUrl: './tab2-chil1.component.css'
})
export class Tab2Chil1Component implements OnChanges{
  @Input() currentStatusCollapse: string = statusCollapse.expandAll;
  @Output() changeStatusCollapse1: EventEmitter<string> = new EventEmitter<string>;

  defautCollapse: number [] = [0,1,2];
  changeCollapse: number [] = this.defautCollapse;

  Expland: Record <string, boolean> = {
    isIconCollapse1: true,
    isIconCollapse2: true,
    isIconCollapse3: true,
  }

  setExpland(status:string = statusCollapse.expandAll): void {
    Object.keys(this.Expland).map(item =>
     this.Expland [item] = (status == statusCollapse.expandAll)
     )
  }


  // Bắt sự thay đổi của statusCollapse mới nhất từ TabParent
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['currentStatusCollapse'] !== changes['currentStatusCollapse'].currentValue){
      if(changes['currentStatusCollapse'].currentValue == statusCollapse.expandAll){
        this.changeCollapse = this.defautCollapse;
        this.setExpland(statusCollapse.expandAll)
        return;
      }
      if(changes['currentStatusCollapse'].currentValue == statusCollapse.collapseAll){
        this.changeCollapse = [];
        this.setExpland(statusCollapse.collapseAll)
        return;
      }
    }

  }


  // Bắt sự thay đổi của Collapse chuyển hóa thành statusCollapse gửi lại TabPrent
  onChangesCollapse(event:number|number []): void {
    if(typeof event == 'number'){return};
      console.log(event);
    if(event.length === this.defautCollapse.length){
        this.changeStatusCollapse1.emit(statusCollapse.expandAll);
    }
    else if(event.length === 0){
      this.changeStatusCollapse1.emit(statusCollapse.collapseAll)
    }else{
    this.changeStatusCollapse1.emit(statusCollapse.activeBoth);
    }
    this.changeCollapse = event;
  }
}
