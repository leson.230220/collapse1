import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Tab2Chil2Component } from './tab2-chil2.component';

describe('Tab2Chil2Component', () => {
  let component: Tab2Chil2Component;
  let fixture: ComponentFixture<Tab2Chil2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Tab2Chil2Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Tab2Chil2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
