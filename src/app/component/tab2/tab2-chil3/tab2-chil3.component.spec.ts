import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Tab2Chil3Component } from './tab2-chil3.component';

describe('Tab2Chil3Component', () => {
  let component: Tab2Chil3Component;
  let fixture: ComponentFixture<Tab2Chil3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Tab2Chil3Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Tab2Chil3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
