import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, } from '@angular/core';
import { statusCollapse } from '../../../types/config';
import { ImfomationService } from '../../../service/imfomation-service.service';
import { Imformation, addImformation } from '../../../types/types';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { log } from 'console';

@Component({
  selector: 'app-tab3-child1',
  templateUrl: './tab3-child1.component.html',
  styleUrl: './tab3-child1.component.css'
})
export class Tab3Child1Component implements OnInit, OnChanges {

  constructor(private imformationService: ImfomationService, private fb: FormBuilder) { }
  listData: Imformation[] = []

  clonedData: Imformation = {
    id: '',
    name: '',
    age: '',
    gender: '',
    eyeColor: '',
    phone: '',
    address: '',
    registered: ''
  }

  date: Date = new Date();



  @Input() currentStatusCollapse: string = statusCollapse.expandAll;
  @Output() changeStatusCollapse1: EventEmitter<string> = new EventEmitter<string>;

  myForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    age: ['', Validators.required],
    gender: ['', Validators.required],
    eyeColor: ['', Validators.required],
    phone: ['', Validators.required],
    address: ['', Validators.required],
    registered: ['']
  });

  ngOnChanges(changes: SimpleChanges): void {
    // if (changes['myForm.value'] != changes['myForm'].currentValue){

    // }
  }

  item: addImformation = {
    name: "",
    age: "",
    gender: "",
    eyeColor: "",
    phone: "",
    address: "",
    registered: new Date()
  }


  eyeColorArr: {}[] = [
    { label: 'Blue', value: 'Blue' },
    { label: 'Black', value: 'Black' },
    { label: 'Green', value: 'Green' }
  ]

  genderArr: {}[] = [
    { label: 'Male', value: 'Male' },
    { label: 'Fermale', value: 'Fermale' },
  ]

  displayNone: boolean = true;
  disableButton: boolean = false;

  ngOnInit(): void {
    this.handleGetData();
  }

  handleGetData(): void {
    console.log("đây là call Get");

    this.imformationService.getData().subscribe({
      next: (v) => {
        this.listData = v
        console.log(v);

      },
      error: (e) => {
        alert(e);
      }
    })
  }

  onRowEditInit(data: Imformation): void {
    this.clonedData = { ...data };
    this.handleClickButton();
  }

  onRowEditSave(data: any, ri: number): void {
    this.handleClickButton();
    if (Object.keys(data) === Object.keys(this.clonedData)) {
      return;
    }
    this.imformationService.updateData(data).subscribe({
      next: (v) => {
        console.log(v);
        alert('Cập nhật thành công!')
        this.listData[ri] = v;
      },
      error: (e) => {
        console.log(e);

      }
    })
    this.clonedData = {
      id: '',
      name: '',
      age: '',
      gender: '',
      eyeColor: '',
      phone: '',
      address: '',
      registered: ''
    };
  }

  onRowEditCancel(data: Imformation, index: number) {
    this.listData[index] = this.clonedData;
    // this.clonedData = {};
    this.handleClickButton();
  }

  hanldeDeleteDataRow(data: Imformation, index: number) {
    this.imformationService.deleteData(data).subscribe({
      next: (v) => {
        console.log(v);
        alert('Xóa thành công!')
        delete this.listData[index];
      },
      error: (e) => {
        console.log(e);
      }
    })
  }

  handleClickButton() {
    this.disableButton = !this.disableButton;
  }

  handleUAddDataRow() {
    this.item = { ...this.myForm.value, registered: this.item.registered }
    
    this.imformationService.addData(this.item).subscribe({
      next: (v) => {
        alert('Thêm dữ liệu thành công!')
        console.log(v);
        this.listData.push(v)
        this.handleResetStateItem();
      },
      error: (e) => {
        console.log(e);
      }

    })
  }

  onClickAdd() {
    this.displayNone = !this.displayNone;
    this.handleResetStateItem();
  }

  handleCheckNullData(data: Imformation): boolean {
    let result: boolean = false;
    if(!data.name || !data.address || !data.age || !data.eyeColor || !data.gender ){
      return result = true;
      
    }
    return result
  }

  handleCheckNullValue(data: string | Date): boolean {
    // let result: boolean = false;
    if (!data) {
      return true;
    }
    return false;
  }

  handleCheckNullAdd(): boolean {
    if(!this.myForm.value.name || !this.myForm.value.age ||
       !this.myForm.value.gender || !this.myForm.value.eyeColor ||
        !this.myForm.value.phone || !this.myForm.value.address){
          return true
    }
    return false;
  }

  handleResetStateItem() {
    this.myForm.reset();
    this.item = {
      name: "",
      age: "",
      gender: "",
      eyeColor: "",
      phone: "",
      address: "",
      registered: new Date()
    }
  }

  onDateChange(event: Date) {
    // this.item.registered =  event.toLocaleDateString('en-GB');
    console.log(event);
    console.log("iiiiii");
    console.log(this.item.registered);
  }




}
