import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Tab3Child3Component } from './tab3-child3.component';

describe('Tab3Child3Component', () => {
  let component: Tab3Child3Component;
  let fixture: ComponentFixture<Tab3Child3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Tab3Child3Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Tab3Child3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
