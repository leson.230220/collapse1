import { Component, EventEmitter, Input, Output,  } from '@angular/core';
import { statusCollapse } from '../../types/config';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.component.html',
  styleUrl: './tab3.component.css'
})
export class Tab3Component {
  @Input() currentStatus: string = statusCollapse.expandAll;
  @Input() childStatusCollapse1: string = ''
  @Input() childStatusCollapse2: string = ''
  @Input() childStatusCollapse3: string = ''
  @Input() activeTabKey: number = 0

  @Output() currentStatusChange: EventEmitter <string> = new EventEmitter<string>;
  @Output() changeTabChild: EventEmitter <number> = new EventEmitter <number>;

  onChangeStatus(statusCollapse: string){
    this.currentStatusChange.emit(statusCollapse);    
  }

  onchangeTabChild(keyChild: number) {
    this.changeTabChild.emit(keyChild);
  }
}
