import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IListParentTab, listTabObj, statusCollapse } from '../types/config';

@Component({
  selector: 'app-root-component',
  templateUrl: './root.component.html',
  styleUrl: './root.component.css'
})
export class RootComponent implements OnChanges{
   // Dữ liệu mặc định của tab khi mới vào
   listTabParent: IListParentTab [] = [...listTabObj]

   // Trạng thái của up down collapse
   currentStatusCollapse: string = this.listTabParent[0].children[0].statusCollapse
 
   //Trạng thái mặc định của Disable All Up Down
   isDisableBtnCollapse: boolean = false;
 
   // Trạng thái mặc định của tab
   activeKeyParentTab: number = this.listTabParent[0].key
   activeKeyChildTab: number = this.listTabParent[0].children[0].key
 

  //
  ngOnChanges(changes: SimpleChanges): void {
    
  }
 
 // hàm nhận index từ parent
  onChangeParentTab(keyParent: number) :void{
   console.log(keyParent);
   
     this.activeKeyParentTab = keyParent;
     this.listTabParent[this.activeKeyParentTab].children.map(child => {
       if(child.inActive == true) {
         this.activeKeyChildTab = child.key;
         this.currentStatusCollapse = child.statusCollapse;
         this.isDisableBtnCollapse = child.isDisableBtnCollapse;
         console.log(this.currentStatusCollapse);
         
       }
     })
  }
 
 
 // Hàm nhận index từ child
  onChangeChildTab(keyChild: number): void {
    console.log(keyChild);
    
     this.activeKeyChildTab = keyChild;
     this.listTabParent[this.activeKeyParentTab].children.map(child => {
       if(child.key == this.activeKeyChildTab) {
         child.inActive = true;
         this.currentStatusCollapse = child.statusCollapse;
         this.isDisableBtnCollapse = child.isDisableBtnCollapse;
       }else{
         child.inActive = false
       }
     })
  }
 
 
  // Hàm nhận statusCollapse từ Child
  onChangeStatusCollapse(statusCollapse: string): void {
     this.listTabParent[this.activeKeyParentTab].children[this.activeKeyChildTab].statusCollapse = statusCollapse;
     this.currentStatusCollapse = statusCollapse;
  }
 
  // Hàm statusCollapse.expandAll
  handleStatusExpandAll(): void {
   this.listTabParent[this.activeKeyParentTab].children[this.activeKeyChildTab].statusCollapse = statusCollapse.expandAll;
   this.currentStatusCollapse = statusCollapse.expandAll;
  }
 
  handleStatusCollapseAll(): void {
   this.listTabParent[this.activeKeyParentTab].children[this.activeKeyChildTab].statusCollapse = statusCollapse.collapseAll;
   this.currentStatusCollapse = statusCollapse.collapseAll;
  }
  
}
