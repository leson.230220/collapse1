import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Imformation } from '../types/types';
import { register } from 'module';

@Injectable({
  providedIn: 'root'
})
export class ImfomationService {

  constructor(private http:HttpClient) {}

  getData():Observable<any> {
    return this.http.get<any> (
      `https://server-deployment-yvii.onrender.com/lesson2`
    )
  }

  addData(data: any): Observable<any> {
    const item = {...data,
       registered : data.registered.toLocaleDateString('en-GB')}
       console.log(item);
       
    return this.http.post<any> (`https://server-deployment-yvii.onrender.com/lesson2`, item)
  }

  updateData(data:any):Observable<any> {
    const item = {...data}
    if(typeof data.registered == 'string') {}
    else{
      item.registered = data.registered.toLocaleDateString('en-GB')
    }
    return this.http.put<any> (`https://server-deployment-yvii.onrender.com/lesson2/${item.id}`, item )
  }

  deleteData(data: Imformation):Observable<any> {
    return this.http.delete<any> (`https://server-deployment-yvii.onrender.com/lesson2/${data.id}`)
  }

}
