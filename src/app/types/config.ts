export const statusCollapse = {
    collapseAll: 'collapseAll',
    expandAll: 'expandAll',
    activeBoth: 'activeBoth'
}

export interface IListParentTab {
    name: string,
    key: number,
    children: childrenTab []
}

export interface childrenTab {
    name: string,
    key: number,
    isDisableBtnCollapse: boolean,
    statusCollapse: string,
    inActive: boolean
}

export const listTabObj: IListParentTab [] = [
    {
        name: 'Tab 1',
        key: 0,
        children: [
            {
                name: 'Tab 1.1',
                key: 0,
                isDisableBtnCollapse: false,
                statusCollapse: statusCollapse.expandAll,
                inActive: true
            }, 
            {
                name: 'Tab 1.2',
                key: 1,
                isDisableBtnCollapse: false,
                statusCollapse: statusCollapse.expandAll,
                inActive: false
            },
            {
                name: 'Tab 1.3',
                key: 2,
                isDisableBtnCollapse: true,
                statusCollapse: statusCollapse.expandAll,
                inActive: false
            },

        ]
    },
    {
        name: 'Tab 2',
        key: 1,
        children: [
            {
                name: 'Tab 2.1',
                key: 0,
                isDisableBtnCollapse: false,
                statusCollapse: statusCollapse.expandAll,
                inActive: true
            }, 
            {
                name: 'Tab 2.2',
                key: 1,
                isDisableBtnCollapse: false,
                statusCollapse: statusCollapse.expandAll,
                inActive: false
            },
            {
                name: 'Tab 2.3',
                key: 2,
                isDisableBtnCollapse: false,
                statusCollapse: statusCollapse.expandAll,
                inActive: false
            },

        ]
    },
    {
        name: 'Tab 3',
        key: 2,
        children: [
            {
                name: 'Tab 3.1',
                key: 0,
                isDisableBtnCollapse: true,
                statusCollapse: statusCollapse.expandAll,
                inActive: true
            }, 
            {
                name: 'Tab 3.2',
                key: 1,
                isDisableBtnCollapse: false,
                statusCollapse: statusCollapse.expandAll,
                inActive: false
            },
            {
                name: 'Tab 3.3',
                key: 2,
                isDisableBtnCollapse: false,
                statusCollapse: statusCollapse.expandAll,
                inActive: false
            },

        ]
    }
]