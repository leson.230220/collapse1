export interface Imformation {
    id: string,
    name: string,
    age: string,
    gender: string,
    eyeColor: string,
    phone: string,
    address: string,
    registered: string
}

export interface addImformation {
    name: string,
    age: string,
    gender: string,
    eyeColor: string,
    phone: string,
    address: string,
    registered: Date
}

